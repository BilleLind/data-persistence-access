package com.example.datapersistenceaccess.runner;

import com.example.datapersistenceaccess.repositories.interfaces.CustomerRepository;
import com.example.datapersistenceaccess.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import java.util.ArrayList;

@Component
public class PgAppRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public PgAppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws RuntimeException {
        // test all methods here
        customerRepository.test();

        System.out.println("1. Read all the customers in the database, (Id, first name, last name, country, postal code, phone number, email)");
        ArrayList<Customer> customers = (ArrayList<Customer>)customerRepository.findAll();
        for (Customer customer : customers) {
            System.out.println(customer.toString());
        }
        System.out.println();

        System.out.println("2. Read a specific customer from the database (by Id), should display everything listed in the above point");
        System.out.println(customerRepository.findById(59) + "\n");

        System.out.println("3. Read a specific customer by name. HINT: LIKE keyword can help for partial matches");
        System.out.println(customerRepository.findByName("er pers") + "\n");

        System.out.println("4. Return a page of customers from the database. This should take in limit and offset as parameters and make use");
        System.out.println(" of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above should be reused");
        customers = (ArrayList<Customer>)customerRepository.getCustomersByName("e", 3, 2);
        for (Customer customer : customers) {
            System.out.println(customer.toString());
        }
        System.out.println();

        System.out.println("5. Add a new customer to the database. You also need to add only the fields listed above (our customer object");
        customerRepository.insert(new Customer(0,"per", "persen", "norway", "5267", "66663332", "email@.no"));
        customers = (ArrayList<Customer>)customerRepository.getCustomersByName("per", 3, 2);
        for (Customer customer : customers) {
            System.out.println(customer.toString());
        }
        System.out.println();

        System.out.println("6. Update an existing customer");
        customerRepository.update(new Customer (10, "newEduardo44444", "newMartins", "USA", "12345", "612042941", "awesome@java.com"));
        System.out.println(customerRepository.findById(10) + "\n");

        System.out.println("7. Return the country with the most customers");
        System.out.println(customerRepository.findCountryWithMostCustomers() + "\n");

        System.out.println("8. Customer who is the highest spender (total in invoice table is the largest)");
        System.out.println(customerRepository.findBiggestSpender() + "\n");

        System.out.println("9. For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context");
        System.out.println(" means the genre that corresponds to the most tracks from invoices associated to that customer");
        System.out.println(customerRepository.findMostPopularGenreForCustomer(12) + "\n");


    }
}
