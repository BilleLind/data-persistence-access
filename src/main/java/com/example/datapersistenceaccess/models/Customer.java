package com.example.datapersistenceaccess.models;

public record Customer (
    int id,
    String firstName,
    String lastName,
    String country,
    String postalCode,
    String phone,
    String email) {

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", first_name='" + firstName + '\'' +
                ", last_name='" + lastName + '\'' +
                ", country='" + country + '\'' +
                ", postal_code='" + postalCode + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
