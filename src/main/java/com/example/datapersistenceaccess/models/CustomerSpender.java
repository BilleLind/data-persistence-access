package com.example.datapersistenceaccess.models;

public record CustomerSpender (
    int id,
    String first_name,
    String last_name,
    String country,
    String postal_code,
    String phone,
    String email,
    double totalSum
){}
