package com.example.datapersistenceaccess.models;

public record CustomerGenre(
        int id,
        String genre,
        int purchases
) {
}
