package com.example.datapersistenceaccess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataPersistenceAccessApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataPersistenceAccessApplication.class, args);
    }

}
