package com.example.datapersistenceaccess.repositories.interfaces;

import java.util.List;

public interface CRUDRepository<T, U> {
    /*
    *   This method Queries the database, for all occurrences of the database equivalent of the specified Class
    *   @return       A List of the specified class Object. eg. Customer.
    *   @exception    SQLException if the database can't be queried/reached
    *   */
    List<T> findAll();

    /*
    *   @param id    is the unique id to be Queried for eg. customer_id
    *   @return      Object which contains the id. eg. Customer
    *   @exception   SQLException if the database can't be queried/reached
    */
    T findById(U id);

    /*
    *   @param object   the instantiated Class that are to be inserted into the database. eg. Customer.
    *   @return         Returns 1 for success
    *   @exception      SQLException if the database can't be queried/reached
    */
    int insert(T object);

    /*
    *   @param object   the instantiated Class that are to be updated into the database. eg. Customer.
    *   @return         Returns 1 for success
    *   @exception      SQLException if the database can't be queried/reached
    * */
    int update(T object);

    // duplicate/redundant
    int delete(T object);

    /*
     *   @param id       the of the row that are to be removed/deleted from the database. eg. customer_id.
     *   @return         Returns 1 for success
     *   @exception      SQLException if the database can't be queried/reached
     * */
    int deleteById(U id);
}