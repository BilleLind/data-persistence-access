package com.example.datapersistenceaccess.repositories.interfaces;

import com.example.datapersistenceaccess.models.Customer;
import com.example.datapersistenceaccess.models.CustomerCountry;
import com.example.datapersistenceaccess.models.CustomerGenre;
import com.example.datapersistenceaccess.models.CustomerSpender;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer> {
    /*
     * Database connection tester
     * @return void              prints out "Connected to Postgres..."
     * @exception SQLException   if the database cannot be reached or bad url, username or password was provided
     * */
    void test();

     /*
     *  @param name             the customer's name, partial or full.
     *  @return Customer        returns the most likely customer by that name
     *  @exception SQLException if the database can't be reached or queried
     * */
    Customer findByName(String name);

     /*
     *  Retrieve a list of customer with the possibility for pagination.
     *
     *  @param name                 the customer's name, partial or full.
     *  @param limit                the number of results to be returned
     *  @param offset               the number of results to be skipped
     *  @return List<Customer>      returns a list of matched customers.
     *  @exception SQLException     if the database can't be reached or queried
      * */
    List<Customer> getCustomersByName(String name, int limit, int offset);

    /*
    *   Retrieve the country with the most customers along with the number of customers
    *
    *   @return CustomerCountry     an object with the name of the country and count of customers.
    *   @exception SQLException     if the database can't be reached or queried
    * */
    CustomerCountry findCountryWithMostCustomers();

    /*
    *   Calculates the highest spending customer through invoices
    *
    *   @return CustomerSpender     A Class Object equal to Customer with the total sum of their purchases
    *   @exception SQLException     if the database can't be reached or queried
     * */
    CustomerSpender findBiggestSpender();


    /*
    *   Retrieves the most popular genre of a specific customer, calculated through the customers invoice's totals,
    *   matched with the genres through the invoice_line table.
    *
    *   @param id                   the customers id
    *   @return customerGenre       The genre's name, total purchases with the possibility of more than one genre
    *                               and total purchases in the case of a tie
    *   @exception SQLException     if the database can't be reached or queried
    * */
    CustomerGenre findMostPopularGenreForCustomer(int id);
}