package com.example.datapersistenceaccess.repositories;

import com.example.datapersistenceaccess.models.Customer;
import com.example.datapersistenceaccess.models.CustomerCountry;
import com.example.datapersistenceaccess.models.CustomerGenre;
import com.example.datapersistenceaccess.models.CustomerSpender;
import com.example.datapersistenceaccess.repositories.interfaces.CustomerRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImp implements CustomerRepository {
    private final String url;
    private final String username;
    private final String password;

    /*
    *   Database environment values retrieve
    *   Stored in application.properties
    * */
    public CustomerRepositoryImp(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public List<Customer> findAll() {
        String sql = "SELECT * FROM customer";
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customers.add(new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                ));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Customer findById(Integer id) {
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        Customer customer = null;
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public Customer findByName(String name) {
        String sql = "SELECT * FROM customer WHERE CONCAT(first_name,' ',last_name) LIKE ? LIMIT 1";
        Customer customer = null;
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + name + "%");
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public List<Customer> getCustomersByName(String name, int limit, int offset) {
        ArrayList<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer WHERE CONCAT(first_name,' ',last_name) LIKE ? ORDER BY first_name LIMIT ? OFFSET ?";
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + name + "%");
            statement.setInt(2, limit);
            statement.setInt(3, offset);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
               customers.add ( new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
               ));

            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public int insert(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int update (Customer customer) {
            String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
            int result = 0;
            try(Connection conn = DriverManager.getConnection(url, username,password)) {
                // Write statement
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1,customer.firstName());
                statement.setString(2, customer.lastName());
                statement.setString(3, customer.country());
                statement.setString(4, customer.postalCode());
                statement.setString(5, customer.phone());
                statement.setString(6, customer.email());
                statement.setInt(7, customer.id());
                // Execute statement
                result = statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return result;
            }

    public CustomerCountry findCountryWithMostCustomers() {
        String sql = "SELECT count(*) as Customers, country FROM customer GROUP BY country ORDER BY Customers DESC LIMIT 1";
        CustomerCountry customerCountry = null;
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customerCountry = new CustomerCountry(
                        result.getString("country"),
                        result.getInt("customers")
                );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
       return customerCountry;
    }

    public CustomerSpender findBiggestSpender() {
        String sql = """
                select sum(total) as total_sum,\s
                invoice.customer_id FROM invoice\s
                INNER join customer ON customer.customer_id = invoice.customer_id
                GROUP BY invoice.customer_id ORDER BY total_sum DESC LIMIT 1""";
        CustomerSpender customerSpender = null;
        int id = -1;
        double totalSum = 0;
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                id = result.getInt("customer_id");
                totalSum = result.getDouble("total_sum");
            }
            Customer customer = findById(id);
            customerSpender = new CustomerSpender(
                customer.id(),
                customer.firstName(),
                customer.lastName(),
                customer.country(),
                customer.postalCode(),
                customer.phone(),
                customer.email(),
                totalSum
            );
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }

    public CustomerGenre findMostPopularGenreForCustomer(int id) {
        String sql = """
                SELECT count(*) as purchases, g.*\s
                from invoice as i, invoice_line as l, track as t, genre as g\s
                where i.invoice_id = l.invoice_id\s
                and l.track_id = t.track_id\s
                and t.genre_id = g.genre_id\s
                and customer_id = 12
                group by g.genre_id\s
                order by purchases desc\s
                fetch first 1 rows With Ties;""";
        ArrayList<CustomerGenre> genres = new ArrayList<>();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                genres.add (new CustomerGenre(
                    id,
                    result.getString("name"),
                    result.getInt("purchases")
                ));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        // combine the genre's in case of tie
        if (genres.size()>1) {
            CustomerGenre g1 = genres.get(0);
            CustomerGenre g2 = genres.get(1);
            String combinedGenreName = g1.genre() + "/ " + g2.genre();
            return new CustomerGenre(id, combinedGenreName, g1.purchases());
        }
        return genres.get(0);
    }

    public int delete(Customer customer) {
        String sql = "DELETE FROM customer WHERE customer_id = ?";
        int id = customer.id();
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int deleteById(Integer id) {
        String sql = "DELETE FROM customer WHERE customer_id = ?";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void test() {
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            System.out.println("Connected to Postgres...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}