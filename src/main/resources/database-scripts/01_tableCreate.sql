
/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS Superhero;

CREATE TABLE Superhero (
	superhero_id SERIAL PRIMARY KEY ,
    superhero_name varchar(50) NOT NULL,
    superhero_alias varchar(50) NOT NULL,
	superhero_origin VARCHAR(70) NOT NULL
);

DROP TABLE IF EXISTS Assistant;

CREATE TABLE Assistant (
	assistant_id SERIAL PRIMARY KEY ,
    assistant_name varchar(50) NOT NULL
);


DROP TABLE IF EXISTS Power;

CREATE TABLE Power (
	power_id SERIAL PRIMARY KEY ,
    power_name varchar(50) NOT NULL,
	power_description varchar(255) NOT NULL
);
