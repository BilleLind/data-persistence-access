
ALTER TABLE assistant
	DROP CONSTRAINT fk_superhero,
    ADD CONSTRAINT fk_superhero FOREIGN KEY (superhero_id) REFERENCES superhero (superhero_id);
	

/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS superhero_power;

CREATE TABLE superhero_power (
    superhero_id int REFERENCES superhero,
    power_id int REFERENCES power,
    PRIMARY KEY (superhero_id, power_id)
);