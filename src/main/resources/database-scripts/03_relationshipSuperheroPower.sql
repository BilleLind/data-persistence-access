ALTER TABLE assistant
	DROP CONSTRAINT fk_superhero,
    ADD CONSTRAINT fk_superhero FOREIGN KEY (superhero_id) REFERENCES superhero (superhero_id);