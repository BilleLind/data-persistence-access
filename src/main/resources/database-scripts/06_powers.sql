TRUNCATE power cascade;

INSERT INTO power (power_id, power_name, power_description) VALUES (1, 'High IQ', 'Smart enough to create gadget for combat');

INSERT INTO power (power_id, power_name, power_description) VALUES (2, 'Laser Eyes', 'Can incinerate stuff');

INSERT INTO power (power_id, power_name, power_description) VALUES (3, 'Super Strength', 'Can lift stuff and punch really Hard');

INSERT INTO power (power_id, power_name, power_description) VALUES (4, 'Spidy sense', 'An Sixth sense');

/* SELECT * from superhero; */

/* One power to many */
INSERT INTO superhero_power(power_id, superhero_id) VALUES(1, 1);
INSERT INTO superhero_power(power_id, superhero_id) VALUES(1, 3);

/* many power to  one */
INSERT INTO superhero_power(power_id, superhero_id) VALUES(2, 2);
INSERT INTO superhero_power(power_id, superhero_id) VALUES(2, 3);

/* one power to one */
INSERT INTO superhero_power(power_id, superhero_id) VALUES(4, 3);
