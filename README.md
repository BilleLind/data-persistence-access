<h1>Assignment 2 data persistence access  </h1>

<h2> Table of contects </h2>

- Background
- Install
- Usage
- Maintainers
- License
- Authors and acknowledgment

<h2> Background </h2>
In the assignment we created a database-scripts map for all the scripts from appendix A, According to the specifications
(data-persistence-access\src\main\resources\database-scripts). With these scripts you would be able to create a database, setup some tables, give relationships and add data in the tables, or update or delete data.


In the second part we created a Spring Boot application to read, search and manipulate a database. With functionalities as search a specific customer, add a new customer, update an existing customer and specific searching for the country with the most customers or the customer with the highest spending. You would also be able to search what a customer most pupular genre is. 

The application is tested, but not 100% covered.

The structure of the source files is as followed:

|| scr\test\java\com\example\datapersistenceaccess

- DataPersistenceAccessApplicationTests.java

||Src\main\java\com\example\datapersistenceaccess


- DataPersistenceAccessApplication.java

- Repositories (map)

-----> CustomerRepositoryImp.java

    - interfaces (map)
    
    ------> CustomerRepository.java

    ------> CRUDRepository.java

- runner (map)

-----> PgAppRunner.java

- models  (map)

-----> Customer

-----> CustomerCountry

-----> CustomerGenre

-----> CustomerSpender


||Src\resources

- application.properties

- database-scripts (map)

-----> 01_tableCreate.sql

-----> 02_relationshipSuperheroAssistant.sql

-----> 03_relationshipSuperheroPower.sql

-----> 04_insertSuperheroes.sql

-----> 05_insertAssistants.sql

-----> 06_powers.sql

-----> 07_updateSuperhero.sql

-----> 08_deleteAssistant.sql

<h2> Install </h2>

- Install java 17

- Install Postgres SQL driver dependency

- Install Postgres and PgAdmin

- Install Intellij Ultimate

- Clone repository


<h2> usage </h2>
In the DataPersistenceAccessApplication class you could search, insert, delete or update data from the datebase as you like.

<h2> Maintainers </h2>
@trygvejohannessen88, @BilleLind and @bartomen
<h2> License </h2>
The project is open-source. Feel free to use it in your own projects, as long as credit the work.

<h2> Authors and Acknowledgmen </h2>
Code author: Anders Bille Lind, Trygve Johannessen, Bart van Dongen.

Assignment given by: Livinus Obiora Nweke, Lecturer at Noroff University College.
